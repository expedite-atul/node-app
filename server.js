`use strict`;
//connect to hapi server localhost:3000
const Hapi = require('hapi')
const server = Hapi.server({
    port:3000,
    host:'localhost'
})
//Connect to mongodb
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/atul_api')
    .then(()=>console.log('connected to MongoDB....'))
    .catch(()=>console.error('could not connect to MongoDB',err))
//create a schema and then a collection in mongodb
const detailsSchema = new mongoose.Schema({
    name: [String],
    address: [String],
    city: String
    
});    
//Modeling the DB collection in mongoDB
const Details = mongoose.model('Details',detailsSchema);
async function createDetails(){
    const details= new Details({
        name :'Atul Singh',
        address :'H Block GreaterNoida',
        city :'Greater Noida'
    }) ;
    //saving a document in your collection
    const result = await details.save();
    console.log(result);

}
createDetails();

//routing HapiJS    
server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {

        return 'Hapi server at route path';
    }
});

server.route({
    method: 'GET',
    path: '/{name}',
    handler: (request, h) => {

        return 'Hello,server initiated using HapiJS ' + encodeURIComponent(request.params.name) + '!';
    }
});

const init = async () => {

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();

