# node-app  
# A node application project using HapiJS 
# MongoDB as Database

## Steps to initiate the project 

### Open terminal in Ubuntu (ctrl+alt+t)

### mkdir node-app

### cd node-app

### sudo npm i -g nodemon

### npm init --yes (yes because of default initialization of npm "can use npm init")
            Wrote to /home/user/node-app/package.json:

            {
              "name": "node-app",
              "version": "1.0.0",
              "description": "",
              "main": "index.js",
              "scripts": {
                "test": "echo \"Error: no test specified\" && exit 1"
              },
              "keywords": [],
              "author": "",
              "license": "ISC"
            }
### npm install --save hapi@17.x.x
            node-app@1.0.0 /home/user/node-app
            ├─┬ hapi@17.8.1
            │ ├── accept@3.1.3
            │ ├── ammo@3.0.3
            │ ├── boom@7.2.2
            │ ├── bounce@1.2.2
            │ ├── call@5.0.3
            │ ├── catbox@10.0.5
            │ ├─┬ catbox-memory@3.1.4
            │ │ └── big-time@2.0.1
            │ ├── heavy@6.1.2
            │ ├── hoek@6.0.1
            │ ├─┬ joi@14.0.4
            │ │ └─┬ isemail@3.2.0
            │ │   └── punycode@2.1.1
            │ ├─┬ mimos@4.0.2
            │ │ └── mime-db@1.37.0
            │ ├── podium@3.1.5
            │ ├── shot@4.0.7
            │ ├── somever@2.0.0
            │ ├─┬ statehood@6.0.8
            │ │ ├── cryptiles@4.1.3
            │ │ └─┬ iron@5.0.6
            │ │   └── b64@4.1.2
            │ ├─┬ subtext@6.0.11
            │ │ ├── content@4.0.6
            │ │ ├─┬ pez@4.0.5
            │ │ │ └─┬ nigel@3.0.4
            │ │ │   └── vise@3.0.1
            │ │ └── wreck@14.1.3
            │ ├── teamwork@3.0.2
            │ └── topo@3.0.3
            └── mongoose@5.0.1  extraneous

### npm i mongoose@5.0.1 (specific version will be added to the project)
            node-app@1.0.0 /home/user/node-app
          `       -- mongoose@5.0.1  extraneous
          
### code . (using VS Code as an IDE)
            After entering in VS Code a tree structure of Directories will be already created as follows
            NODE-APP
            |-node_modules
                  .....
            |-package.json
 
### create a server.js file in your root folder [server.js]
            to create (ctrl+N)
            to rename as server.js (ctrl+shift+n)
            this server.js file wil have code for connecting to hapiJSserver as well as connecting and creating collection               and inserting documents in MongoDB
            code//
            `use strict`;
            //connect to hapi server localhost:3000
            const Hapi = require('hapi')
            const server = Hapi.server({
                port:3000,
                host:'localhost'
            })
            //Connect to mongodb
            const mongoose = require('mongoose')
            mongoose.connect('mongodb://localhost/atul_api')
                .then(()=>console.log('connected to MongoDB....'))
                .catch(()=>console.error('could not connect to MongoDB',err))
            //create a schema and then a collection in mongodb
            const detailsSchema = new mongoose.Schema({
                name: [String],
                address: [String],
                city: String

            });    
            //Modeling the DB collection in mongoDB
            const Details = mongoose.model('Details',detailsSchema);
            async function createDetails(){
                const details= new Details({
                    name :'Atul Singh',
                    address :'H Block GreaterNoida',
                    city :'Greater Noida'
                }) ;
                //saving a document in your collection
                const result = await details.save();
                console.log(result);

            }
            createDetails();

            //routing HapiJS    
            server.route({
                method: 'GET',
                path: '/',
                handler: (request, h) => {

                    return 'Hapi server at route path';
                }
            });

            server.route({
                method: 'GET',
                path: '/{name}',
                handler: (request, h) => {

                    return 'Hello,server initiated using HapiJS ' + encodeURIComponent(request.params.name) + '!';
                }
            });

            const init = async () => {

                await server.start();
                console.log(`Server running at: ${server.info.uri}`);
            };

            process.on('unhandledRejection', (err) => {

                console.log(err);
                process.exit(1);
            });

            init();
### Run Server.js file by typing node 
            $-> nodemon server.js            
### Output from server.js 
            [nodemon] 1.18.9
            [nodemon] to restart at any time, enter `rs`
            [nodemon] watching: *.*
            [nodemon] starting `node server.js`
            Server running at: http://localhost:3000
            connected to MongoDB....
            { name: [ 'Atul Singh' ],
              address: [ 'H Block GreaterNoida' ],
              _id: 5c495cdfa51fb83d73389634,
              city: 'Greater Noida',
              __v: 0 }
